import sys
import time
import rospy
import numpy as np

from std_msgs.msg import Empty as EmptyMsg
from sensor_msgs.msg import NavSatFix
from mavros_msgs.msg import State
from mavros_msgs.msg import HomePosition
from mavros_msgs.msg import GlobalPositionTarget
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped
from mavros_msgs.msg import PositionTarget
from mavros_msgs.srv import CommandBool
from mavros_msgs.srv import CommandLong
from mavros_msgs.srv import CommandTOL
from mavros_msgs.srv import SetMode

from .utils import *

class Executor:
    def __init__(self):
        rospy.Subscriber(
            "/mavros/global_position/global",
            NavSatFix,
            self.__gpsCallbackFnc
        )
        rospy.Subscriber(
            "/mavros/local_position/odom",
            Odometry,
            self.__localPoseCallbackFnc
        )
        rospy.Subscriber(
            "/mavros/state",
            State,
            self.__stateCallbackFnc
        )
        rospy.Subscriber(
            "/mavros/home_position/home",
            HomePosition,
            self.__homeCallbackFnc
        )
        rospy.Subscriber(
            "/aruco_pose",
            PoseStamped,
            self.__arucoCallback
        )
        self.gpsPointPub = rospy.Publisher('/mavros/setpoint_raw/global', GlobalPositionTarget, queue_size=1)
        self.localPointPub = rospy.Publisher('/mavros/setpoint_raw/local', PositionTarget, queue_size=1)
        self.arm_service = rospy.ServiceProxy('/mavros/cmd/arming', CommandBool)
        self.takeoff_service = rospy.ServiceProxy('/mavros/cmd/takeoff', CommandTOL)
        self.set_mode_service = rospy.ServiceProxy('/mavros/set_mode', SetMode)
        self.land_service = rospy.ServiceProxy('/mavros/cmd/land', CommandTOL)
        self.command_service = rospy.ServiceProxy('/mavros/cmd/command', CommandLong)
        self.__aruco_pose = None
        self.__gps_msg = None
        self.__state_msg = None
        self.__local_msg = None
        self.__home_msg = None
        self.is_takeoff = False
        self.is_armed = False
        self.is_guided = False
        self.is_init = False
        self.__silents = False

    def __localPoseCallbackFnc(self, msg):
        self.__local_msg = msg
        self.initCheck()

    def initCheck(self):
        if not self.is_init:
            is_init = True
            if self.__local_msg is None :
                is_init = False
            if self.__state_msg is None :
                is_init = False
            if self.__gps_msg is None :
                is_init = False
            if self.__home_msg is None :
                is_init = False
            if is_init:
                self.is_init = True
                self.takeoffCheck()

    def __gpsCallbackFnc(self, msg):
        self.__gps_msg = msg
        self.initCheck()

    def __homeCallbackFnc(self, msg):
        self.__home_msg = msg
        self.initCheck()

    @property
    def gsp_msg(self):
        return self.__gps_msg
    
    @property
    def state_msg(self):
        return self.__state_msg
    
    @property
    def local_msg(self):
        return self.__local_msg
    
    @property
    def home_msg(self):
        return self.__home_msg

    def takeoffCheck(self):
        if self.is_init:
            # if self.__state_msg.system_status == 4 and np.abs(self.__gps_msg.altitude - self.start_gps_point.altitude) > 0.05:
            if self.__state_msg.system_status == 4:
                self.is_takeoff = True
            else:
                self.is_takeoff = False

    def __stateCallbackFnc(self, msg):
        self.__state_msg = msg
        self.is_armed = msg.armed
        self.is_guided = msg.guided
        self.takeoffCheck()
        self.initCheck()
    
    def __arucoCallback(self, msg):
        if self.is_init:
            self.__aruco_pose = self.getCurrentLocalPoint() + np.array([msg.pose.position.x, msg.pose.position.y, msg.pose.position.z])

    def goToLocalPoint(self, x, y, z, yaw, threshold = 0.1, asynchronous: bool = False):
        msg = PositionTarget()
        msg.coordinate_frame = 1
        msg.type_mask = 2552
        msg.position.x = x
        msg.position.y = y
        msg.position.z = z
        msg.yaw = yaw

        self.localPointPub.publish(msg)
        if asynchronous:
            return
        while not self.inLocalPoint(x, y, z, threshold):
            time.sleep(0.1)

    def getArucoMarkerLocalPose(self) -> np.array:
        return self.__aruco_pose

    def goToGPSPoint(self, latitude: float, longitude: float, altitude: float, yaw: float, threshold: float = 0.1, asynchronous: bool = False):

        msg = GlobalPositionTarget()
        msg.latitude = latitude
        msg.longitude = longitude
        msg.yaw = yaw
        msg.altitude = np.float32(altitude)
        msg.coordinate_frame = GlobalPositionTarget.FRAME_GLOBAL_REL_ALT
        msg.type_mask = 8 + 16 + 32 + 64 + 128 + 256 + 1024 + 2048
        self.gpsPointPub.publish(msg)
        if asynchronous:
            return
        while not self.inGPSPoint(latitude, longitude, altitude, threshold):
            time.sleep(0.1)

    def inLocalPoint(self, x, y, z, threshold):
        if not self.__silents:
            print("In local_pose: {}, {}, {}".format(x, y, z))
        return self.pointDistance(np.array([x, y, z])) <= threshold

    def inGPSPoint(self, latitude, longitude, altitude, threshold = 0.1):
        if not self.__silents:
            print("In gps: {}, {}, {}".format(latitude, longitude, altitude))
        return self.gpsPointDistance(latitude, longitude, altitude) <= threshold
    
    def go_home(self, working_altitude: float, threshold: float = 0.1, asynchronous: bool = False):
        if not self.__silents:
            print("Go Home")
        self.goToGPSPoint(
            latitude = self.__home_msg.geo.latitude,
            longitude = self.__home_msg.geo.longitude,
            altitude = working_altitude,
            yaw = 0,
            threshold = threshold,
            asynchronous= asynchronous
        )
    
    def inHome(self, threshold: float = 0.1):
        if not self.__silents:
            print("In home")
        return self.inGPSPoint(self.__home_msg.geo.latitude, self.__home_msg.geo.longitude, self.__gps_msg.altitude - self.__home_msg.geo.altitude, threshold)

    def getCurrentLocalPoint(self) -> np.ndarray:
        lp = self.local_msg.pose.pose.position
        v2 = np.array([lp.x,lp.y,lp.z])
        return v2

    def pointDistance(self, point: np.array):
        lp = self.local_msg.pose.pose.position
        v2 = np.array([lp.x,lp.y,lp.z])
        err = v2 - point
        if not self.__silents:
            print("distance to point: ", np.linalg.norm(err))        
        return np.linalg.norm(err)

    def getCurrentGPSPoint(self):
        gps = {"latitude": self.__gps_msg.latitude, "longitude": self.__gps_msg.longitude, "altitude": self.__gps_msg.altitude - self.__home_msg.geo.altitude, "yaw": 0}
        return gps

    def gpsPointDistance(self, latitude, longitude, altitude):
        '''
        giving degrees to approximate values in metres

        Earth radius was taken as  6371 km
        '''
        v1 = np.array([latitude, longitude, altitude])
        v2 = np.array([self.__gps_msg.latitude, self.__gps_msg.longitude, self.__gps_msg.altitude - self.__home_msg.geo.altitude])
        err = v2 - v1
        k1 = METER_TO_DEGREE
        k2 = k1*np.cos(latitude + err[0]/2)*2
        K = np.array([k1,k2,1.0])
        if not self.__silents:
            print("---------------------")
            print("distance to point: ", np.linalg.norm(err*K))        
        return np.linalg.norm(err*K)

    def waitTillReady(self):
        while not self.is_init:
            print("Waiting for new msgs...")
            time.sleep(1)
        while not (self.is_guided and self.is_armed):
            time.sleep(1)
            if (not self.is_guided) and self.is_armed:
                print("Waiting for GUIDED mode")
            elif (not self.is_armed) and self.is_guided:
                print("Waiting for ARM")
            else:
                print("Waiting for ARM and GUIDED mode")

    def takeoff(self, altitude, wait = 5):
        if not self.is_takeoff:
            self.takeoff_service(altitude = altitude)
            print("TAKEOFF")
            time.sleep(wait)

    def land(self):
        if self.is_takeoff:
            self.land_service()
            print("LANDING")

    def gimbalFollowGPSPoint(self, latitude, longitude, altitude):
        self.command_service(
            command = 201,
            param5 = latitude,
            param6 = longitude,
            param7 = altitude + self.__home_msg.geo.altitude
        )

if __name__=="__main__":
    try:
        rospy.init_node('test_gps', disable_signals = True)
        tg = Executor()
        
    except rospy.ROSInterruptException:
        pass
