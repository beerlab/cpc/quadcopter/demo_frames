import os
import csv
import numpy as np

METER_TO_DEGREE = 111194.92664455873
METER_TO_DEGREE_LONG = 55597.463322

def parseWaypointFile(file, DEFAULT_WORKING_ALTITUDE) -> list:
    WAYPOINT_ARRAY = []
    with open(os.path.join("/workspace/ros_ws/src/demo_frames/waypoints",file),'r') as file_obj:
        csv_reader = csv.reader(file_obj, delimiter='\t')
        for i in range(2):
            next(csv_reader)
        for row in csv_reader:
            if len(row) >= 10:
                WAYPOINT_ARRAY.append({"latitude": float(row[8]), "longitude": float(row[9]), "altitude": DEFAULT_WORKING_ALTITUDE, "yaw": 0})
    return WAYPOINT_ARRAY

def generateCirclePoints(latitude,longitude,altitude,radius,step) -> list:
    ## Generate circle points 
        CIRCLE_WAYPOINT_ARRAY = []
        R_LAT = radius/METER_TO_DEGREE
        R_LONG = radius/(METER_TO_DEGREE*2*np.cos(latitude))
        for angle in np.arange(0,2*np.pi,2*np.pi/step):
            wp = {"latitude": latitude + R_LAT*np.sin(angle), 
                  "longitude": longitude + R_LONG*np.cos(angle), 
                  "altitude": altitude, 
                  "yaw": 0}
            CIRCLE_WAYPOINT_ARRAY.append(wp)
        return CIRCLE_WAYPOINT_ARRAY

def generateCircleLocalPoints(x,y,z,radius,step):
    ## Generate circle points 
        CIRCLE_WAYPOINT_ARRAY = []
        for angle in np.arange(0,2*np.pi,2*np.pi/step):
            wp = {"x": radius*np.sin(angle), 
                  "y": radius*np.cos(angle), 
                  "z": z, 
                  "yaw": 0}
            CIRCLE_WAYPOINT_ARRAY.append(wp)
        return CIRCLE_WAYPOINT_ARRAY

def globalPointFromRelativeLocalMove(wp,north_move,east_move):
    nwp = wp.copy()
    nwp["latitude"] += north_move/METER_TO_DEGREE
    nwp["longitude"] += east_move/METER_TO_DEGREE_LONG
    return nwp
