import os
import sys
import rospy
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from demo_frames.executor import Executor

## Frame params =======================

DEFAULT_WORKING_ALTITUDE = 15
DEFAULT_TAKEOFF_ALTITUDE = 2
DEFAULT_DIST_THRESHOLD = 0.5 # meters

## ====================================


if __name__=="__main__":
    try:
        parser = argparse.ArgumentParser()

        parser.add_argument('-t', '--takeoff_altitude', type = float, default = DEFAULT_TAKEOFF_ALTITUDE, help = "Takeoff altitude")
        parser.add_argument('-w', '--working_altitude', type = float, default = DEFAULT_WORKING_ALTITUDE, help = "Working altitude")
        parser.add_argument('--threshold', type = float,  default = DEFAULT_DIST_THRESHOLD, help = "It's radius of sphere for gps points. When drone in that sphere about desired gps point, then sends next point.")

        args = parser.parse_args()

        rospy.init_node('frame1', disable_signals = True)
        print("Start Frame 1.")
        ex = Executor()
        ex.waitTillReady()
        ex.takeoff(altitude = args.takeoff_altitude)
        ex.goToGPSPoint(
            ex.gsp_msg.latitude,
            ex.gsp_msg.longitude,
            args.working_altitude,
            0.0,
            args.threshold
        )
        print("Frame 1 done.")
        
        
    except rospy.ROSInterruptException:
        pass
