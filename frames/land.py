import os
import sys
import rospy
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from demo_frames.executor import Executor


if __name__=="__main__":
    try:
        rospy.init_node('land', disable_signals = True)
        ex = Executor()
        ex.waitTillReady()
        ex.land()
        
        
    except rospy.ROSInterruptException:
        pass