import os
import sys
import rospy
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from demo_frames.executor import Executor
from demo_frames.utils import *

## Frame params =======================

SAFE_LAND_MOVE_NORTH = 3.0
SAFE_LAND_MOVE_EAST = -3.0
DEFAULT_WORKING_ALTITUDE = 15
BOREHOLE_ALTITUDE = 4
CIRCLE_MOVE_RADIUS = 2
CIRCLE_STEP = 10
CIRCLE_NUMBER = 2
DEFAULT_DIST_THRESHOLD = 0.5 # meters

## ====================================

if __name__=="__main__":
    try:
        ## Parse params
        parser = argparse.ArgumentParser()
        parser.add_argument('--north_safe_land_move', type = float, default = SAFE_LAND_MOVE_NORTH, help = "Move to north for save landing after circle move")
        parser.add_argument('--east_safe_land_move', type = float, default = SAFE_LAND_MOVE_EAST, help = "Move to east for save landing after circle move")
        parser.add_argument('-s', '--step', type = float, default = CIRCLE_STEP, help = "Number of circle step" )
        parser.add_argument('-n', '--number', type = float, default = CIRCLE_NUMBER, help = "Number of circle" )
        parser.add_argument('-r', '--radius', type = float, default = CIRCLE_MOVE_RADIUS, help = "Radius of circle" )
        parser.add_argument('-f', '--file', type = str, required = True, help = "File with waypoints from CGQ. Else used points from script" )
        parser.add_argument('-w', '--working_altitude', type = float, default = DEFAULT_WORKING_ALTITUDE, help = "Working altitude")
        parser.add_argument('-b', '--borehole_altitude', type = float, default = BOREHOLE_ALTITUDE, help = "Borehole altitude")
        parser.add_argument('--threshold', type = float,  default = DEFAULT_DIST_THRESHOLD, help = "It's radius of sphere for gps points. When drone in that sphere about desired gps point, then sends next point.")

        args = parser.parse_args()

        ## Start ROS node
        rospy.init_node('frame3', disable_signals = True)
        print("Start Frame 3.")
        ex = Executor()
        ex.waitTillReady() ## GUIDED and ARM

        ## Get waypoints from file
        if not args.file is None:
            WAYPOINT_ARRAY = parseWaypointFile(args.file, args.working_altitude)
        
        ## Generate points for circle move
        CIRCLE_WAYPOINT_ARRAY = generateCirclePoints(
                WAYPOINT_ARRAY[0]["latitude"],
                WAYPOINT_ARRAY[0]["longitude"],
                args.borehole_altitude,
                args.radius,
                args.step
        )

        ## Gimbal look to global point on
        ex.gimbalFollowGPSPoint(WAYPOINT_ARRAY[0]["latitude"],WAYPOINT_ARRAY[0]["longitude"],0)

        ## Point to point move: move to point above the borehole
        ex.goToGPSPoint(
            latitude = WAYPOINT_ARRAY[0]["latitude"],
            longitude = WAYPOINT_ARRAY[0]["longitude"],
            altitude = args.working_altitude,
            yaw = WAYPOINT_ARRAY[0]["yaw"],
            threshold = DEFAULT_DIST_THRESHOLD
            )
        
        print("Go DOWN...")
        ## Point to point move: move down to borehole
        ex.goToGPSPoint(
            latitude = WAYPOINT_ARRAY[0]["latitude"],
            longitude = WAYPOINT_ARRAY[0]["longitude"],
            altitude = args.borehole_altitude,
            yaw = WAYPOINT_ARRAY[0]["yaw"],
            threshold = DEFAULT_DIST_THRESHOLD
            )

        ## Circle move
        print("Circle move")
        for i in range(args.number):
            for wp in CIRCLE_WAYPOINT_ARRAY:
                ex.goToGPSPoint(
                latitude = wp["latitude"],
                longitude = wp["longitude"],
                altitude = wp["altitude"],
                yaw = wp["yaw"],
                threshold = DEFAULT_DIST_THRESHOLD
                )

        ## Go back to the center of borehole
        ex.goToGPSPoint(
            latitude = WAYPOINT_ARRAY[-1]["latitude"],
            longitude = WAYPOINT_ARRAY[-1]["longitude"],
            altitude = args.borehole_altitude,
            yaw = WAYPOINT_ARRAY[-1]["yaw"],
            threshold = DEFAULT_DIST_THRESHOLD
            )
        
        ## Go to safe point before landing
        wp = globalPointFromRelativeLocalMove(
                WAYPOINT_ARRAY[-1],
                args.north_safe_land_move,
                args.east_safe_land_move
            )
        ex.goToGPSPoint(
            latitude = wp["latitude"],
            longitude = wp["longitude"],
            altitude = args.borehole_altitude,
            yaw = wp["yaw"],
            threshold = DEFAULT_DIST_THRESHOLD
            )
        print("Frame 3 done.")
        
        
    except rospy.ROSInterruptException:
        pass
