import os
import sys
import time
import rospy
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from demo_frames.executor import Executor
from demo_frames.utils import *

## Frame params =======================

DEFAULT_WORKING_ALTITUDE = 8
DEFAULT_TAKEOFF_ALTITUDE = 2
DEFAULT_DIST_THRESHOLD = 0.7 # meters
DEFAULT_CIRCLE_STEP = 30
DEFAULT_CIRCLE_NUMBER = 2
DEFAULT_CIRCLE_RADIUS = 5
DEFAULT_BAREHOLE_ALTITUDE  = 7
## ====================================


def circle_move(ex, args):
    local_pose = ex.local_msg.pose.pose.position
    CIRCLE_WAYPOINT_ARRAY = generateCircleLocalPoints(
        local_pose.x,
        local_pose.y,
        args.borehole_altitude,
        args.radius,
        args.step
    )

    for i in range(args.number):
        for wp in CIRCLE_WAYPOINT_ARRAY:
            ex.goToLocalPoint(
                x = local_pose.x + wp["x"],
                y = local_pose.y + wp["y"],
                z = wp["z"],
                yaw = wp["yaw"],
                threshold = args.threshold
            )

## Get waypoints from file
def get_waypoints(filepath: str, working_altitude: float) -> list:
    return parseWaypointFile(filepath, working_altitude)

# Get gps point of hole
def detect_borehole_gps(filepath: str, working_altitude: float) -> dict:
    print("Detecting borehole")
    return get_waypoints(filepath, working_altitude)[0]

def search_borehole(coast_gps_path: list, filepath: str, threshold: float) -> dict:
    print("Searching borehole")
    borehole_gps = None
    for wp in coast_gps_path:
        ex.goToGPSPoint(
            latitude = wp["latitude"],
            longitude = wp["longitude"],
            altitude = wp["altitude"],
            yaw = wp["yaw"],
            threshold = threshold,
            asynchronous= True
        )
        while not ex.inGPSPoint(wp["latitude"], wp["longitude"], wp["altitude"], threshold):
            borehole_gps = detect_borehole_gps(filepath, wp["altitude"])
            if not borehole_gps is None:
                return borehole_gps
    return None

def go_home(filepath: str, working_altitude: float, threshold: float):
    wp = get_waypoints(filepath, working_altitude)[0]
    ex.goToGPSPoint(
        latitude = wp["latitude"],
        longitude = wp["longitude"],
        altitude = wp["altitude"],
        yaw = wp["yaw"],
        threshold = threshold,
        asynchronous= True
    )
    ex.land()

def inspect_borehole(ex: Executor, gps: dict, working_altitude: float, threshold: float):
    ex.gimbalFollowGPSPoint(gps["latitude"], gps["longitude"], gps["altitude"])
    ex.goToGPSPoint(
        latitude = gps["latitude"],
        longitude = gps["longitude"],
        altitude = working_altitude,
        yaw = gps["yaw"],
        threshold = threshold,
        asynchronous= False
    )
    ex.goToGPSPoint(
        latitude = gps["latitude"],
        longitude = gps["longitude"],
        altitude = args.borehole_altitude,
        yaw = gps["yaw"],
        threshold = threshold,
        asynchronous= False
    )
    circle_move(ex, args)


def searchAruco(ex: Executor, working_altitude: float, threshold: float) -> np.array:
    ex.go_home(working_altitude, threshold, asynchronous=True)
    while not ex.inHome(threshold):
        aruco_pose = ex.getArucoMarkerLocalPose()
        if not aruco_pose is None:
            current_gps = ex.getCurrentGPSPoint()
            ex.goToGPSPoint(
                latitude = current_gps["latitude"],
                longitude = current_gps["longitude"],
                altitude = working_altitude,
                yaw = current_gps["yaw"],
                threshold = threshold,
                asynchronous= False
            )
            return aruco_pose
    return None

if __name__=="__main__":
    try:
        parser = argparse.ArgumentParser()

        parser.add_argument('-f', '--file', type = str, required = True, help = "File with waypoints from CGQ. Else used points from script" )
        parser.add_argument('-bhf', '--borehole_file', type = str, required = True, help = "File with waypoints from CGQ. Else used points from script" )
        parser.add_argument('-t', '--takeoff_altitude', type = float, default = DEFAULT_TAKEOFF_ALTITUDE, help = "Takeoff altitude")
        parser.add_argument('-w', '--working_altitude', type = float, default = DEFAULT_WORKING_ALTITUDE, help = "Working altitude")
        parser.add_argument('--threshold', type = float,  default = DEFAULT_DIST_THRESHOLD, help = "It's radius of sphere for gps points. When drone in that sphere about desired gps point, then sends next point.")
        parser.add_argument('-s', '--step', type = float, default = DEFAULT_CIRCLE_STEP, help = "Number of circle step" )
        parser.add_argument('-n', '--number', type = float, default = DEFAULT_CIRCLE_NUMBER, help = "Number of circle" )
        parser.add_argument('-r', '--radius', type = float, default = DEFAULT_CIRCLE_RADIUS, help = "Radius of circle" )
        parser.add_argument('-b', '--borehole_altitude', type = float, default = DEFAULT_BAREHOLE_ALTITUDE, help = "Borehole altitude")

        args = parser.parse_args()

        rospy.init_node('Demo_frame', disable_signals = True)
        print("Start DemoFrame")
        ex = Executor()
        ex.waitTillReady()
        ex.takeoff(altitude = args.takeoff_altitude)
        
        # Estimate path from YOLO:
        coast_gps_path = get_waypoints(args.file, args.working_altitude)

        # Go via points from YOLO
        borehole_gps = search_borehole(coast_gps_path, args.borehole_file, args.threshold)
        if not borehole_gps is None:
            inspect_borehole(ex, borehole_gps, args.working_altitude, args.threshold)
        print("Search aruco")
        aruco_pose = searchAruco(ex, args.working_altitude, args.threshold)
        if aruco_pose is None:
            print("ARUCO doesn't found. Go home")
            ex.go_home(args.working_altitude, args.threshold)
        else:
            print("Go to aruco point")
            target_point = aruco_pose
            ex.goToLocalPoint(target_point[0], target_point[1], target_point[2], 0.0, args.threshold)
        print("Landing")
        ex.land()
        print("DemoFrame Done")
        
        
    except rospy.ROSInterruptException:
        pass
