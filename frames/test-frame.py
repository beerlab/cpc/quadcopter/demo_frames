import os
import sys
import rospy
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from demo_frames.executor import Executor
from demo_frames.utils import *

## Frame params =======================

WORKING_ALTITUDE = 15
BOREHOLE_ALTITUDE = 4
THRESHOLD = 0.5 # meters
CIRCLE_NUMBER = 2
## ====================================

if __name__=="__main__":
    try:
        ## Parse params
        parser = argparse.ArgumentParser()

        parser.add_argument('-w', '--working_altitude', type = float, default = WORKING_ALTITUDE, help = "Working altitude")
        parser.add_argument('-b', '--borehole_altitude', type = float, default = BOREHOLE_ALTITUDE, help = "Borehole altitude")
        parser.add_argument('--threshold', type = float,  default = THRESHOLD, help = "It's radius of sphere for gps points. When drone in that sphere about desired gps point, then sends next point.")

        args = parser.parse_args()

        ## Start ROS node
        rospy.init_node('test', disable_signals = True)
        print("Start test Frame.")
        ex = Executor()
        ex.waitTillReady()  ## GUIDED and ARM


        local_pose = ex.local_msg.pose.pose.position
        
        CIRCLE_WAYPOINT_ARRAY = generateCircleLocalPoints(
                local_pose.x,
                local_pose.y,
                7,
                5,
                30
        )

        for i in range(CIRCLE_NUMBER):
            for wp in CIRCLE_WAYPOINT_ARRAY:
                ex.goToLocalPoint(
                    x = local_pose.x + wp["x"],
                    y = local_pose.y + wp["y"],
                    z = wp["z"],
                    yaw = wp["yaw"],
                    threshold = THRESHOLD
                )

        print("Test Frame done.")
        
        
    except rospy.ROSInterruptException:
        pass