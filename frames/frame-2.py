import os
import sys
import csv
import rospy
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from demo_frames.executor import Executor
from demo_frames.utils import *

## Frame params =======================

DEFAULT_WORKING_ALTITUDE = 15
DEFAULT_DIST_THRESHOLD = 0.5 # meters
## ====================================

if __name__=="__main__":
    try:
        ## Parse params
        parser = argparse.ArgumentParser()

        parser.add_argument('-f', '--file', type = str, required = True, help = "File with waypoints from CGQ. Else used points from script" )
        parser.add_argument('-w', '--working_altitude', type = float, default = DEFAULT_WORKING_ALTITUDE, help = "Working altitude")
        parser.add_argument('--threshold', type = float,  default = DEFAULT_DIST_THRESHOLD, help = "It's radius of sphere for gps points. When drone in that sphere about desired gps point, then sends next point.")

        args = parser.parse_args()

        ## Start ROS node
        rospy.init_node('frame2', disable_signals = True)
        print("Start Frame 2.")
        ex = Executor()
        ex.waitTillReady()  ## GUIDED and ARM

        ## Get waypoints from file
        if not args.file is None:
            WAYPOINT_ARRAY = parseWaypointFile(args.file, args.working_altitude)

        for wp in WAYPOINT_ARRAY:
            ex.goToGPSPoint(
                latitude = wp["latitude"],
                longitude = wp["longitude"],
                altitude = wp["altitude"],
                yaw = wp["yaw"],
                threshold = DEFAULT_DIST_THRESHOLD
                )
        print("Frame 2 done.")
        
        
    except rospy.ROSInterruptException:
        pass